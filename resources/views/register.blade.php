<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Form</title>
	</head>
	<body>
		<h1>Buat Account Baru</h1>
		<h2>Sign Up Form</h2>
		<form action="welcome" method="POST">
            @csrf
			<label for="firstame">First Name:</label> <br /><br />
			<input type="text" name="firstname" id="firstname" /> <br /><br />

			<label for="lastname">Last Name:</label> <br /><br />
			<input type="text" name="lastname" id="lastname" /> <br /><br />

			<label for="gender">Gender:</label> <br /><br />
			<input type="radio" id="male" name="gender" value="male" />
			<label for="male">Male</label><br />
			<input type="radio" id="female" name="gender" value="female" />
			<label for="female">Female</label> <br /><br />

			<label for="nationality">Nationality:</label> <br /><br />
			<select name="nationality" id="nationality">
				<option value="indonesian">Indonesian</option>
				<option value="singaporean">Singaporean</option>
				<option value="malaysian">Malaysian</option>
				<option value="australian">Australian</option>
			</select>
			<br /><br />

			<label for="language">Language Spoken:</label> <br /><br />
			<input type="checkbox" id="language1" name="language1" value="bahasa" />
			<label for="language1"> Bahasa Indonesia</label><br />
			<input type="checkbox" id="language2" name="language2" value="english" />
			<label for="language2"> English</label><br />
			<input type="checkbox" id="language3" name="language3" value="otherlanguage" />

			<label for="language3"> Other</label> <br /><br />

			<label for="bio">Bio:</label> <br /><br />
			<textarea rows="8" cols="50" name="bio"></textarea> <br />

			<button type="submit" value="Submit">Sign Up</button>
		</form>
	</body>
</html>